let express 				= require("express"),
	bodyParser 				= require("body-parser"),
	request 				= require('request'),
	mongoose 				= require('mongoose'),
	passport				= require('passport'),
	LocalStrategy 			= require('passport-local').Strategy,
	passportLocalMongoose 	= require('passport-local-mongoose'),
	User 					= require('./models/user'),
	Transaction 			= require('./models/transaction'),
	CarteMise				= require('./models/carteMise'),
	fs						= require('fs'),
	Flash					= require('connect-flash'),
	schedule 				= require('node-schedule');

//https://gitlab.com/dword4/nhlapi
let today = new Date();
let debut = today.getFullYear() + '-' + (today.getMonth()-1) + '-' + today.getDate();
let fin = today.getFullYear() + '-' + (today.getMonth()-1) + '-' + (today.getDate()+1);
const mises = require('./PartieEnCours');
let sportID = {
	hockey: 'nhl',
	baseball: 'mlb',
	basketball: 'nba',
	mma: 'mma',
	boxe: 'wbc'
}
mongoose.connect("mongodb://localhost/plan-de-match");
// mongoose.connect("mongodb://localhost:27017/plan-de-match", {useNewUrlParser: true});
let app = express()
app.use(require("express-session")({
	secret: "Plan de match",
	resave: false,
	saveUninitialized:false
}));

app.use(express.static("public"));
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.json());
app.use(passport.initialize()); //initialise les librairies passport
app.use(passport.session());

// app.use(flash());
//lire les datas de la session
passport.use(new LocalStrategy(User.authenticate()));

app.use((req, res, next) =>{
	res.locals.currentUser = req.user;
	next();
})
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//route pour accueil
app.get('/', (req, res) => {
	res.render('index');
});
app.get('/commentjouer', (req, res) => {
	res.render('commentjouer');
});
app.get('/inscription', (req, res) => {
	res.render('inscription')
});

app.post('/inscription', (req, res) => {
	User.register(new User({username: req.body.username, email:req.body.email, solde: 0}), req.body.password, (err, user) =>{
		if(err){
			console.log("erreur = " + err);
			return res.render('inscription');
		}
		//ce qui log l'usager à son profil
		passport.authenticate('local')(req,res, () => {
			res.redirect('/profil');
		})
	})
})

app.get('/connexion', (req, res) => {
	res.render('connexion');
})
//gère l'authentification
app.post('/connexion', passport.authenticate('local', {
	successRedirect:'/profil',
	failureRedirect:'/connexion'
	}), (req, res) => {
});
app.get('/logout', (req, res) =>{
	req.logout();
	res.redirect('/');
})
const isLoggedIn = (req, res, next) =>{
	if(req.isAuthenticated()){
		return next();
	}
	res.redirect('/connexion');
}
app.get('/profil',isLoggedIn, (req, res) => {
	res.render('profil')
})

//////////////////////////////////////////////////////////////
let apiKey = 'e214b9b3e40a60f8fe9bc8ec0a93dd63-39bc661a-2cdc251a';
let domain = 'www.vanessadpilote.com';
let mailgun = require('mailgun-js')({ apiKey: apiKey, domain: domain });
let template = fs.readFileSync('./views/templates/billing1.html');
let template2 = fs.readFileSync('./views/templates/billing2.html');
let template3 = fs.readFileSync('./views/templates/billing3.html');
let template4 = fs.readFileSync('./views/templates/billing4.ejs');
//////////////////////////////////////////////////////////////

app.post('/profil',isLoggedIn, (req, res) => {
	res.redirect('/profil')
	let courr = template.toString() + req.body.NomComplet + template2.toString() + (parseFloat(req.body.nouveauSolde)).toFixed(2) + template3.toString() + (parseFloat(req.body.nouveauSolde)).toFixed(2) + template4.toString()
	User.findById(res.locals.currentUser._id, (err, user) => {
		user.solde+= parseFloat(req.body.nouveauSolde)
		user.save();
	})
	const data = {
		from: 'Plan de Match <vanessadpilote@gmail.com>',
		to: req.body.Courriel,
		subject: 'Confirmation ajout de fonds',
		text: 'Testing some Mailgun awesomeness!',
		html:courr

	  };

	  mailgun.messages().send(data, (error, body) => {
		console.log(body);
	  });

})


app.get('/parier', isLoggedIn, (req, res) => {
	res.render('parier');
});

let headers = {
    'x-api-key': 'ce2404d2-4c26-4037-bee6-ad11b7d09c01'
};

//envoi du formulaire pour la confirmation des cartes de mise
app.post('/parier/:sport', (req, res) => {
	User.findById(res.locals.currentUser._id, (err, user) => {
		if(err){
			res.redirect("/profil");
		}
		else{
			Transaction.create(req.body, function(err,transaction){
				if(err){
					console.log("err = " + err);
				}
				else{
					user.solde -= (parseFloat(req.body.montant)).toFixed(2);
					transaction.carteMise.push(req.body)
					user.transactions.push(transaction);
					user.save();
					res.redirect('/profil')
				}
			})
		}
	})
})
//function avec la requête API
app.get('/parier/:sport', (req, res) =>{

	let pariEnCours = [];
	montant = req.query.montant;
	sport = req.params.sport;
	if(req.query.event !== undefined){
		Object.keys(req.query.event).forEach(ev => {
			let arr = req.query.event[ev].split(",");
			pariEnCours.push({
				id: ev,
				name:arr[0],
				odd:arr[1],
				venue:arr[2],
				sportID:arr[3]
			})
		})
	}
	let options = {
		url: 'https://jsonodds.com/api/odds/' + sportID[sport],
		headers: headers
	};
	request(options, (error, response, body) => {
		if(!error && response.statusCode == 200){
			body = JSON.parse(body);
			res.render('parier-choix', {body:body, pariEnCours:pariEnCours, montant:montant, param:sport});
		}
		else{
			console.log(error);
		}
	})
});


const validateCompleted = (eventID, venue) => {
	let win = false;
	let options = {
		url: 'https://jsonodds.com/api/results/' + eventID,
		headers: headers
	};
	request(options, (error, response, body) => {
		if(!error && response.statusCode == 200){
			let score = venue+"Score"
			body = JSON.parse(body);
			body.forEach(re =>{
				if(re.FinalType === "Finished"){
					if(re[score] < re.Homescore){
						console.log("you lose home score");
					}
					else if(re[score] < re.AwayScore){
						console.log("you lose away score");
					}
					else{
						win = true;
					}
				}
			})
		}
		else{
			console.log(error);
		}
	})

	return win;
}
// checkEvent()
let rule = new schedule.RecurrenceRule();

rule.hour = 4;
rule.minute = 00;

let checkEvent = schedule.scheduleJob(rule, () => {
	User.find({ }, (err,user) => {
		if(!err){
			user.forEach(usager => {
				usager.transactions.forEach(trans => {
					if(trans.completed == false){
						trans.carteMise.forEach(carte => {
							carte.team.forEach(te => {
								if(te.id=== '552affc6-206a-416b-bb10-4529f60add21'){
									if(te.name === 'Tampa Bay Rays'){
										transaction.completed == true;
										user.solde += (parseFloat(carte.gain)).toFixed(2);
									}
								}
								if(validateCompleted(te.id, te.venue)){
									user.solde += (parseFloat(carte.gain)).toFixed(2);
									carte.completed = true;
								}

							})
						})
					}
				})
			})
		}
		else{
			throw err;
		}
	})
})

app.listen(80, process.env.IP, () =>{
 	console.log("serveur est parti");
});
