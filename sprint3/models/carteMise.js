let mongoose = require("mongoose");

let carteMiseSchema = new mongoose.Schema({
		team:[],
		montant: Number,
		oddsTotal: Number,
		gain: Number
	})

module.exports = mongoose.model("CarteMise", carteMiseSchema);