let mongoose = require("mongoose");

let transactionSchema = new mongoose.Schema({
		created: {
			type: Date,
			default: Date.now,
		},
		carteMise: [],
		completed: {
			type: Boolean,
			default:false
		}
	})

module.exports = mongoose.model("Transaction", transactionSchema);