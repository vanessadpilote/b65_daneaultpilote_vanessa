let mongoose = require('mongoose');
let passportLocalMongoose = require("passport-local-mongoose");
let Transaction = require('../models/transaction');


let userSchema = new mongoose.Schema({
	username: String,
	email: String,
	password: String,
	solde: Number,
	transactions: []
});






userSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model("User", userSchema);
